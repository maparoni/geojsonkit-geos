# GeoJSONKit+GEOS

Extensions for using the [Geometry Engine - Open Source](https://trac.osgeo.org/geos) with [GeoJSONKit](https://gitlab.com/maparoni/geojsonkit).

Heavily based on [GEOSwift](https://github.com/GEOSwift/GEOSwift).
