// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
  name: "GeoJSONKit+GEOS",
  products: [
    // Products define the executables and libraries produced by a package, and make them visible to other packages.
    .library(
      name: "GeoJSONKit+GEOS",
      targets: ["GeoJSONKit+GEOS"]),
  ],
  dependencies: [
    // Dependencies declare other packages that this package depends on.
    .package(name: "GeoJSONKit", url: "https://gitlab.com/maparoni/geojsonkit.git", from: "0.1.0"),
    .package(url: "https://github.com/maparoni/geos.git", from: "5.0.0"),
  ],
  targets: [
    // Targets are the basic building blocks of a package. A target can define a module or a test suite.
    // Targets can depend on other targets in this package, and on products in packages which this package depends on.
    .target(
      name: "GeoJSONKit+GEOS",
      dependencies: [
        "geos",
        "GeoJSONKit",
    ]),
    .testTarget(
      name: "GeoJSONKit+GEOSTests",
      dependencies: ["GeoJSONKit+GEOS"]),
  ]
)
