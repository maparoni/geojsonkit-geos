import XCTest

import GeoJSONKit_GEOSTests

var tests = [XCTestCaseEntry]()
tests += GeoJSONKit_GEOSTests.allTests()
XCTMain(tests)
