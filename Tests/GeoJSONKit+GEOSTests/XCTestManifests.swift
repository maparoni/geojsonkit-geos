import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(GeoJSONKit_GEOSTests.allTests),
    ]
}
#endif
