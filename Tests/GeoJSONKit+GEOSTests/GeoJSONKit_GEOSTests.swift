import XCTest
@testable import GeoJSONKit_GEOS

final class GeoJSONKit_GEOSTests: XCTestCase {
  func testExample() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct
    // results.
    XCTAssertEqual("Nope}", "Hello, World!")
  }
  
  static var allTests = [
    ("testExample", testExample),
  ]
}
