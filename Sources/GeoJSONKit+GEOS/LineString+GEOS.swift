import geos
import GeoJSONKit

extension GeoJSON.LineString: GEOSObjectInitializable {
  init(geosObject: GEOSObject) throws {
    switch geosObject.type {
    case .lineString,
         .linearRing: break // good
    default:
      throw GEOSError.typeMismatch(actual: geosObject.type, expected: [.lineString, .linearRing])
    }
    try self.init(positions: makePoints(from: geosObject))
  }
}

extension GeoJSON.LineString: GEOSObjectConvertible {
  func geosObject(with context: GEOSContext) throws -> GEOSObject {
    return try makeGEOSObject(with: context, points: positions) { (context, sequence) in
      GEOSGeom_createLineString_r(context.handle, sequence)
    }
  }
}
