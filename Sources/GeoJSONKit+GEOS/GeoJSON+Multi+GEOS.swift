import geos
import GeoJSONKit

extension GeoJSON {
  struct MultiPoint {
    let points: [GeoJSON.Position]
  }

  struct MultiLineString {
    let lines: [GeoJSON.LineString]
  }

  struct MultiPolygon {
    let polygons: [GeoJSON.Polygon]
  }
  
  public struct GeometryCollection {
    let geometries: [GeoJSON.GeometryObject]
  }
}

extension GeoJSON.MultiPoint: GEOSObjectInitializable, GEOSObjectConvertible {
  init(geosObject: GEOSObject) throws {
    guard case .some(.multiPoint) = geosObject.type else {
      throw GEOSError.typeMismatch(actual: geosObject.type, expected: [.multiPoint])
    }
    self.points = try makeGeometries(geometry: geosObject)
  }
  
  func geosObject(with context: GEOSContext) throws -> GEOSObject {
    return try makeGEOSCollection(with: context, geometries: points, type: GEOS_MULTIPOINT)
  }
}

extension GeoJSON.MultiLineString: GEOSObjectInitializable, GEOSObjectConvertible {
  init(geosObject: GEOSObject) throws {
    guard case .some(.multiLineString) = geosObject.type else {
      throw GEOSError.typeMismatch(actual: geosObject.type, expected: [.multiLineString])
    }
    self.lines = try makeGeometries(geometry: geosObject)
  }
  
  func geosObject(with context: GEOSContext) throws -> GEOSObject {
    return try makeGEOSCollection(with: context, geometries: lines, type: GEOS_MULTILINESTRING)
  }
}

extension GeoJSON.MultiPolygon: GEOSObjectInitializable, GEOSObjectConvertible {
  init(geosObject: GEOSObject) throws {
    guard case .some(.multiPolygon) = geosObject.type else {
      throw GEOSError.typeMismatch(actual: geosObject.type, expected: [.multiPolygon])
    }
    self.polygons = try makeGeometries(geometry: geosObject)
  }
  
  func geosObject(with context: GEOSContext) throws -> GEOSObject {
    return try makeGEOSCollection(with: context, geometries: polygons, type: GEOS_MULTIPOLYGON)
  }
}

extension GeoJSON.GeometryCollection: GEOSObjectInitializable, GEOSObjectConvertible {
  init(geosObject: GEOSObject) throws {
    guard case .some(.geometryCollection) = geosObject.type else {
      throw GEOSError.typeMismatch(actual: geosObject.type, expected: [.geometryCollection])
    }
    self.geometries = try makeGeometries(geometry: geosObject)
  }

  func geosObject(with context: GEOSContext) throws -> GEOSObject {
    return try makeGEOSCollection(with: context, geometries: geometries, type: GEOS_GEOMETRYCOLLECTION)
  }
}
