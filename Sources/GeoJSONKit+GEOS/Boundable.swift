import geos
import GeoJSONKit

public protocol Boundable: GeometryConvertible {
  func boundary() throws -> GeoJSON.GeometryObject
}

extension Boundable {
  public func boundary() throws -> GeoJSON.GeometryObject {
    return try performUnaryTopologyOperation(GEOSBoundary_r)
  }
}

extension GeoJSON.Position: Boundable {}
extension GeoJSON.LineString: Boundable {}
extension GeoJSON.LinearRing: Boundable {}
extension GeoJSON.Polygon: Boundable {}
extension GeoJSON.MultiPoint: Boundable {}
extension GeoJSON.MultiLineString: Boundable {}
extension GeoJSON.MultiPolygon: Boundable {}
// GeometryCollection (and by extension, Geometry) is not Boundable
