//
//  GeoJSON+GeometryConvertible.swift
//  
//
//  Created by Adrian Schönig on 02.04.20.
//

import Foundation
import GeoJSONKit

public protocol GeometryConvertible {
  var geometry: GeoJSON.GeometryObject { get }
}

extension GeoJSON.Geometry: GeometryConvertible {
  public var geometry: GeoJSON.GeometryObject { .single(self) }
}

extension GeoJSON.GeometryObject: GeometryConvertible {
  public var geometry: GeoJSON.GeometryObject { self }
}

extension GeoJSON.Position: GeometryConvertible {
  public var geometry: GeoJSON.GeometryObject {
    .single(.point(self))
  }
}

extension GeoJSON.LineString: GeometryConvertible {
  public var geometry: GeoJSON.GeometryObject {
    .single(.lineString(self))
  }
}

extension GeoJSON.LinearRing: GeometryConvertible {
  public var geometry: GeoJSON.GeometryObject {
    .single(.lineString(GeoJSON.LineString(positions: positions)))
  }
}

extension GeoJSON.Polygon: GeometryConvertible {
  public var geometry: GeoJSON.GeometryObject {
    .single(.polygon(self))
  }
}

extension GeoJSON.MultiPoint: GeometryConvertible {
  public var geometry: GeoJSON.GeometryObject {
    .multi(points.map { .point($0) })
  }
}

extension GeoJSON.MultiLineString: GeometryConvertible {
  public var geometry: GeoJSON.GeometryObject {
    .multi(lines.map { .lineString($0) })
  }
}

extension GeoJSON.MultiPolygon: GeometryConvertible {
  public var geometry: GeoJSON.GeometryObject {
    .multi(polygons.map { .polygon($0) })
  }
}

extension GeoJSON.GeometryCollection: GeometryConvertible {
  public var geometry: GeoJSON.GeometryObject {
    return .collection(geometries)
  }
}
