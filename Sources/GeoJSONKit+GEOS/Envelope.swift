import GeoJSONKit

public struct Envelope: Hashable {
  public internal(set) var minX: Double
  public internal(set) var maxX: Double
  public internal(set) var minY: Double
  public internal(set) var maxY: Double
  
  public init(minX: Double, maxX: Double, minY: Double, maxY: Double) {
    precondition(minX <= maxX, "Envelope: minX must not be greater than maxX")
    precondition(minY <= maxY, "Envelope: minY must not be greater than maxY")
    self.minX = minX
    self.maxX = maxX
    self.minY = minY
    self.maxY = maxY
  }
  
  public var minXMaxY: GeoJSON.Position {
    return GeoJSON.Position(x: minX, y: maxY)
  }
  
  public var maxXMaxY: GeoJSON.Position {
    return GeoJSON.Position(x: maxX, y: maxY)
  }
  
  public var minXMinY: GeoJSON.Position {
    return GeoJSON.Position(x: minX, y: minY)
  }
  
  public var maxXMinY: GeoJSON.Position {
    return GeoJSON.Position(x: maxX, y: minY)
  }
  
  public var boundingBox: GeoJSON.BoundingBox {
    return GeoJSON.BoundingBox(positions: [minXMinY, maxXMaxY])
  }
}

extension Envelope: GeometryConvertible {
  public var geometry: GeoJSON.GeometryObject {
    if minX == maxX, minY == maxY {
      return .single(.point(GeoJSON.Position(x: minX, y: minY)))
    } else {
      return .single(.polygon(GeoJSON.Polygon(exterior: GeoJSON.Polygon.LinearRing(
        positions: [
          GeoJSON.Position(x: minX, y: minY),
          GeoJSON.Position(x: maxX, y: minY),
          GeoJSON.Position(x: maxX, y: maxY),
          GeoJSON.Position(x: minX, y: maxY),
          GeoJSON.Position(x: minX, y: minY)]))))
    }
  }
}
