import GeoJSONKit

enum GEOSError: Error {
  case unableToCreateContext
  case libraryError(errorMessages: [String])
  case wkbDataWasEmpty
  case typeMismatch(actual: GEOSObjectType?, expected: [GEOSObjectType])
  case noMinimumBoundingCircle
  
  case tooFewPoints
  case negativeBufferWidth
  case unexpectedEnvelopeResult(GeoJSON.GeometryObject)
}
