import geos
import GeoJSONKit

public protocol ClosednessTestable: GeometryConvertible {
  func isClosed() throws -> Bool
}

extension ClosednessTestable {
  public func isClosed() throws -> Bool {
    return try evaluateUnaryPredicate(GEOSisClosed_r)
  }
}

// GeoJSON.Position is not ClosednessTestable
extension GeoJSON.LineString: ClosednessTestable {}
extension GeoJSON.LinearRing: ClosednessTestable {}
// GeoJSON.Polygon is not ClosednessTestable
// MultiPoint is not ClosednessTestable
extension GeoJSON.MultiLineString: ClosednessTestable {}
// MultiPolygon is not ClosednessTestable
// GeometryCollection (and by extension, Geometry) is not ClosednessTestable
