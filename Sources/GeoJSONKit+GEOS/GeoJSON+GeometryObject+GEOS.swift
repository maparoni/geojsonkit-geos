import GeoJSONKit

extension GeoJSON.GeometryObject: GEOSObjectInitializable {
  init(geosObject: GEOSObject) throws {
    guard let type = geosObject.type else {
      throw GEOSError.libraryError(errorMessages: geosObject.context.errors)
    }
    switch type {
    case .point:
      self = try GeoJSON.Position(geosObject: geosObject).geometry
    case .lineString:
      self = try GeoJSON.LineString(geosObject: geosObject).geometry
    case .linearRing:
      self = try GeoJSON.Polygon.LinearRing(geosObject: geosObject).geometry
    case .polygon:
      self = try GeoJSON.Polygon(geosObject: geosObject).geometry
    case .multiPoint:
      self = try GeoJSON.MultiPoint(geosObject: geosObject).geometry
    case .multiLineString:
      self = try GeoJSON.MultiLineString(geosObject: geosObject).geometry
    case .multiPolygon:
      self = try GeoJSON.MultiPolygon(geosObject: geosObject).geometry
    case .geometryCollection:
      self = try GeoJSON.GeometryCollection(geosObject: geosObject).geometry
    }
  }
}

extension GeoJSON.GeometryObject: GEOSObjectConvertible {
  func geosObject(with context: GEOSContext) throws -> GEOSObject {
    switch self {
    case let .single(.point(position)):
      return try position.geosObject(with: context)
    case let .single(.lineString(lineString)):
      return try lineString.geosObject(with: context)
    case let .single(.polygon(polygon)):
      return try polygon.geosObject(with: context)
      
    case let .multi(geometries):
      if let points = Self.extractPoints(from: geometries), !points.isEmpty {
        return try GeoJSON.MultiPoint(points: points).geosObject(with: context)
      } else if let lines = Self.extractLines(from: geometries), !lines.isEmpty {
        return try GeoJSON.MultiLineString(lines: lines).geosObject(with: context)
      } else if let polygons = Self.extractPolygons(from: geometries), !polygons.isEmpty {
        return try GeoJSON.MultiPolygon(polygons: polygons).geosObject(with: context)
      } else {
        preconditionFailure()
      }

    case let .collection(geometries):
      return try GeoJSON.GeometryCollection(geometries: geometries).geosObject(with: context)
    }
  }
}

fileprivate extension GeoJSON.GeometryObject {
  static func extractPoints(from geometries: [GeoJSON.Geometry]) -> [GeoJSON.Position]? {
    let points = geometries.compactMap { geometry -> GeoJSON.Position? in
      if case let .point(position) = geometry {
        return position
      } else {
        return nil
      }
    }
    return points.isEmpty ? nil : points
  }
  
  static func extractLines(from geometries: [GeoJSON.Geometry]) -> [GeoJSON.LineString]? {
    let lines = geometries.compactMap { geometry -> GeoJSON.LineString? in
      if case let .lineString(line) = geometry {
        return line
      } else {
        return nil
      }
    }
    return lines.isEmpty ? nil : lines
  }
  
  static func extractPolygons(from geometries: [GeoJSON.Geometry]) -> [GeoJSON.Polygon]? {
    let polygons = geometries.compactMap { geometry -> GeoJSON.Polygon? in
      if case let .polygon(polygon) = geometry {
        return polygon
      } else {
        return nil
      }
    }
    return polygons.isEmpty ? nil : polygons
  }
}

