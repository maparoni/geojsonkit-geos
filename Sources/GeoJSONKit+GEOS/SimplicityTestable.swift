import geos
import GeoJSONKit

public protocol SimplicityTestable: GeometryConvertible {
  func isSimple() throws -> Bool
}

extension SimplicityTestable {
  public func isSimple() throws -> Bool {
    return try evaluateUnaryPredicate(GEOSisSimple_r)
  }
}

extension GeoJSON.Position: SimplicityTestable {}
extension GeoJSON.LineString: SimplicityTestable {}
extension GeoJSON.LinearRing: SimplicityTestable {}
extension GeoJSON.Polygon: SimplicityTestable {}
extension GeoJSON.MultiPoint: SimplicityTestable {}
extension GeoJSON.MultiLineString: SimplicityTestable {}
extension GeoJSON.MultiPolygon: SimplicityTestable {}
// GeometryCollection (and by extension, Geometry) is not SimplicityTestable
