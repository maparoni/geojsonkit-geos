import geos
import GeoJSONKit

public struct Circle: Hashable {
  public var center: GeoJSON.Position
  public var radius: Double
  
  public init(center: GeoJSON.Position, radius: Double) {
    self.center = center
    self.radius = radius
  }
}
